#include <stdio.h>

void swap(int *a , int* b) {
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
}

int main() {
    int a = 5;
    int b = 9;

    printf("a is %d\n", a);
    printf("b is %d\n", b);

    swap(&a, &b);

    printf("a is %d\n", a);
    printf("b is %d\n", b);

    return 0;
}