#include <stdio.h>
#include <string.h>
#define MAX 10

#define RECURSIVE_REVERSE

int readArray(int [], int);
void printArray(int [], int);
void reverseArray(int [], int);

void recursiveReverseArray(int [], int, int);
void iterativeReverseArray(int [], int);

int main(void)
{
    int array[MAX], numElements;

    numElements = readArray(array, MAX);
    reverseArray(array, numElements);
    printArray(array, numElements);

    return 0;
}

int readArray(int arr[], int limit)
{
    printf("Enter up to %d integers, terminating with a negative integer.\n", limit);

    char lineBuffer[512] = { 0 };
    if (!fgets(lineBuffer, sizeof(lineBuffer), stdin)) {
        return 0;
    }

    char delimters[] = "\r\n ";
    char* tok = strtok(lineBuffer, delimters);
    int numElements = 0;
    while (tok && numElements < limit) {
        // sscanf can return either -1 (EOF), 0 or 1.
        if (sscanf(tok, "%d", &arr[numElements])) {
            // If the input is negative, end.
            if (arr[numElements] < 0) {
                break;
            }
            ++numElements;
        }
        tok = strtok(0, delimters);
    }

    return numElements;
}

void recursiveReverseArray(int arr[], int headIndex, int tailIndex)
{
    if (tailIndex <= headIndex) { return; }

    int temp = arr[headIndex];
    arr[headIndex] = arr[tailIndex];
    arr[tailIndex] = temp;

    recursiveReverseArray(arr, headIndex + 1, tailIndex - 1);
}

void iterativeReverseArray(int arr[], int size)
{
    for (int i = 0; i < size/2; ++i) {
        int temp = arr[i];
        arr[i] = arr[size - 1 - i];
        arr[size - 1 - i] = temp;
    }
}

void reverseArray(int arr[], int size)
{
#ifdef RECURSIVE_REVERSE
    recursiveReverseArray(arr, 0, size - 1);
#else
    iterativeReverseArray(arr, size);
#endif
}

void printArray(int arr[], int size)
{
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}