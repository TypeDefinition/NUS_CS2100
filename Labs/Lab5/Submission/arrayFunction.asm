# arrayFunction.asm
       .data 
array: .word 8, 2, 1, 6, 9, 7, 3, 5, 0, 4
newl:  .asciiz "\n"

       .text
main:
	la $s0, array
	li $s1, 10
	
	# Print the original content of array
	# setup the parameter(s)
	# call the printArray function
	or $a0, $0, $s0
	or $a1, $0, $s1
	jal printArray

	# Ask the user for two indices
	li   $v0, 5         	# System call code for read_int
	syscall           
	addi $t0, $v0, 0    	# first user input in $t0
 
	li   $v0, 5         	# System call code for read_int
	syscall           
	addi $t1, $v0, 0    	# second user input in $t1

	# Call the findMin function
	# setup the parameter(s)
	# call the function
	sll $t2, $t0, 2
	add $a0, $s0, $t2 # address of first input
	sll $t2, $t1, 2
	add $a1, $s0, $t2 # address of second input
	jal findMin
	or $s2, $0, $v0 # Store address of the min item in $s2.

	# Print the min item
	# place the min item in $t3 for printing
	lw $t3, 0($s2)

	# Print an integer followed by a newline
	li   $v0, 1   		# system call code for print_int
    addi $a0, $t3, 0    # print $t3
    syscall       		# make system call

	li   $v0, 4   		# system call code for print_string
    la   $a0, newl    	# 
    syscall       		# print newline

	# Calculate and print the index of min item
	# Place the min index in $t3 for printing	
	sub $t3, $s2, $s0
	srl $t3, $t3, 2

	# Print the min index
	# Print an integer followed by a newline
	li   $v0, 1   		# system call code for print_int
    addi $a0, $t3, 0    # print $t3
    syscall       		# make system call
	
	li   $v0, 4   		# system call code for print_string
    la   $a0, newl    	# 
    syscall       		# print newline
	
	# End of main, make a syscall to "exit"
	li   $v0, 10   		# system call code for exit
	syscall	       	# terminate program
	

#######################################################################
###   Function printArray   ### 
#Input: Array Address in $a0, Number of elements in $a1
#Output: None
#Purpose: Print array elements
#Registers used: $t0, $t1, $t2, $t3
#Assumption: Array element is word size (4-byte)
printArray:
	addi $t1, $a0, 0	#$t1 is the pointer to the item
	sll  $t2, $a1, 2	#$t2 is the offset beyond the last item
	add  $t2, $a0, $t2 	#$t2 is pointing beyond the last item
l1:	
	beq  $t1, $t2, e1
	lw   $t3, 0($t1)	#$t3 is the current item
	li   $v0, 1   		# system call code for print_int
     	addi $a0, $t3, 0    	# integer to print
     	syscall       		# print it
	addi $t1, $t1, 4
	j l1				# Another iteration
e1:
	li   $v0, 4   		# system call code for print_string
     	la   $a0, newl    	# 
     	syscall       		# print newline
	jr $ra			# return from this function


#######################################################################
###   Student Function findMin   ### 
#Input: Lower Array Pointer in $a0, Higher Array Pointer in $a1
#Output: $v0 contains the address of min item 
#Purpose: Find and return the minimum item 
#              between $a0 and $a1 (inclusive)
#Registers used: $t0, $t1, $t2
#Assumption: Array element is word size (4-byte), $a0 <= $a1
findMin:
	add $v0, $a0, $0 # Current Address
	lw $t0, 0($a0) # Current Value
	
	addi $t4, $a1, 4 # 4 After Last Address
	or $t1, $a0, $0 # Next Address

findLoop:
	addi $t1, $t1, 4 # Next Address += 4
	slt $t3, $t1, $t4
	beq $t3, $0, exitFind

	lw $t2, 0($t1) # Next Value
	slt $t3, $t0, $t2 # Is Next Value Smaller then Current Value?
	bne $t3, $0 findLoop

	# If Next Value < Current Value,
	# Current Address = Next Address, Current Value = Next Value
	or $v0, $0, $t1
	or $t0, $0, $t2
	j findLoop

exitFind:
	jr $ra			# return from this function